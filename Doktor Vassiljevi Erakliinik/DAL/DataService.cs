﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using LINQtoCSV;
using Domain;

namespace DAL
{
    /// <summary>
    /// Service, that acts as DAL. Loads, saves data and also holds the data in memory for other layers to consume
    /// </summary>
    public class DataService : IDataService
    {
        private readonly string _path;
        private readonly CsvContext _cc = new CsvContext();

        private readonly List<Disease> _diseases = new List<Disease>();
        private readonly List<Symptom> _symptoms = new List<Symptom>();
        public List<Disease> Diseases => _diseases;
        public List<Symptom> Symptoms => _symptoms;


        //Linq to CSV configuration / description: http://www.codeproject.com/Articles/25133/LINQ-to-CSV-library#CsvFileDescription
        private readonly CsvFileDescription _desc = new CsvFileDescription()
        {
            SeparatorChar = ',',
            FirstLineHasColumnNames = false,
            EnforceCsvColumnAttribute = true
        };

        /// <summary>
        /// Constructor of data service. Communicates with database and handles loaded data in memory.
        /// Do not confuse with business logic, as this service only loads, saves and moves data, does not manipulate it.
        /// </summary>
        /// <param name="path">Path must be .csv database file</param>
        public DataService(string path)
        {
            _path = path;
            Console.WriteLine($"[{DateTime.Now}] Dr. Vassiljev: Dataservice initialized, loading data...");
            Console.WriteLine($"[{DateTime.Now}] Dr. Vassiljev: Path is {_path}");
            Load();
        }

        public void Load()
        {
            LoadDiseases();
            LoadSymptoms();
            Console.WriteLine($"[{DateTime.Now}] Dr. Vassiljev: Everything should be loaded.");
        }

        public void Save()
        {
            throw new System.NotImplementedException();
        }

        public Symptom GetSymptom(Symptom obj) => _symptoms.FirstOrDefault(s => s.SymptomName.Equals(obj.SymptomName));
        public Symptom GetSymptom(string name) => _symptoms.FirstOrDefault(s => s.SymptomName.Equals(name));

        /// <summary>
        /// Checks if a symptom is already loaded (maybe from another disease)
        /// </summary>
        /// <param name="obj">Newly loaded symptom instance</param>
        /// <returns>True, if is loaded</returns>
        private bool IsSymptomLoaded(Symptom obj) => _symptoms.Any(symptom => symptom.SymptomName.Equals(obj.SymptomName));

        /// <summary>
        /// Loads diseases from database to memory
        /// </summary>
        private void LoadDiseases() => _diseases.AddRange(_cc.Read<DataRow>(_path, _desc).Select(d => new Disease { DiseaseId = d[0].LineNbr, DiseaseName = d[0].Value }).ToList());

        /// <summary>
        /// Small helper to iterate through all loaded diseases and load all the symptoms
        /// </summary>
        private void LoadSymptoms() => _diseases.ForEach(LoadSymptoms);

        /// <summary>
        /// Loads symptoms for a disease
        /// </summary>
        /// <param name="obj">Disease instance</param>
        /// <returns>List of symptom's _diseases</returns>
        private void LoadSymptoms(Disease obj)
        {
            var row = _cc.Read<DataRow>(_path, _desc).ElementAt(obj.DiseaseId - 1); //Reads data row

            //Iterates through row items(skips first item, as it is disease and rest are symptoms)
            foreach (var item in row.GetRange(1, row.Count - 1))
            {
                var symptom = new Symptom { SymptomName = item.Value }; //Creates temporary symptom instance
                // If symptom is already loaded, handles data differently(occurs when symptom was present with any previously loaded disease)
                if (IsSymptomLoaded(symptom))
                {
                    symptom = GetSymptom(symptom); //Finds the previously loaded symptom
                    symptom.Diseases.Add(obj); //Adds the disease to the symptom (inflates virtual List)
                    obj.Symptoms.Add(symptom); //Adds the symptom to the disease (inflates virtual List)
                    continue; //Skip to next iteration
                }
                symptom.Diseases.Add(obj); //Adds the disease to the symptom (inflates virtual List)
                obj.Symptoms.Add(symptom); //Adds the symptom to the disease (inflates virtual List)
                _symptoms.Add(symptom); //Finally add's loaded symptom to data collection
            }
        }
    }
}