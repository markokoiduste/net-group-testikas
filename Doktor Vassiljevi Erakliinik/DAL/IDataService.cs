﻿using System.Collections.Generic;
using Domain;

namespace DAL
{
    public interface IDataService
    {
        /// <summary>
        /// Load all data from database to memory
        /// </summary>
        void Load();

        /// <summary>
        /// Save all data from memory to database
        /// </summary>
        void Save();

        /// <summary>
        /// Finds and returns symptom, if it is loaded
        /// </summary>
        /// <param name="obj">Newly loaded symptom instance</param>
        /// <returns>Previously loaded symptom instance</returns>
        Symptom GetSymptom(Symptom obj);

        /// <summary>
        /// Finds and returns loaded symptom by its name
        /// </summary>
        /// <param name="name">Name property</param>
        /// <returns>Symptom instance</returns>
        Symptom GetSymptom(string name);
    }
}