﻿using System.Collections.Generic;
using Domain;

namespace BLL
{
    public interface IHospitalService
    {
        /// <summary>
        /// 1.1 - Find three most popular diseases, by number of symptoms, ordered by name.
        /// </summary>
        /// <returns>List of 3 popular diseases</returns>
        List<Disease> GetPopularDiseases();

        /// <summary>
        /// 1.2 - Get number of unique symptoms in database
        /// </summary>
        /// <returns>Number of unique symptoms</returns>
        int GetNumberOfUniqueSymptoms();

        /// <summary>
        /// 1.3 - Find three most popular symptoms, by number of diseases it's present in, ordered by name.
        /// </summary>
        /// <returns>List of 3 popular diseases</returns>
        List<Symptom> GetPopularSymptoms();
        
        /// <summary>
        /// Finds disease by list of symptoms
        /// </summary>
        /// <param name="symptoms">Array of symptom names</param>
        /// <returns>List of found diseases</returns>
        List<Disease> FindDiseaseBySymptoms(string[] symptoms);

        /// <summary>
        /// Finds disease by list of symptoms
        /// </summary>
        /// <param name="symptoms">List of symptom instances</param>
        /// <returns>List of found diseases</returns>
        List<Disease> FindDiseaseBySymptoms(List<Symptom> symptoms);

        /// <summary>
        /// Get all diseases from memory
        /// </summary>
        /// <returns>List of all loaded diseases</returns>
        List<Disease> GetAllDiseases();

        /// <summary>
        /// Get all symptoms from memory
        /// </summary>
        /// <returns>List of all loaded symptoms</returns>
        List<Symptom> GetAllSymptoms();

        /// <summary>
        /// Find least popular symptom from given list of symptoms.
        /// Method is necessary for interactive diagnoser to work.
        /// </summary>
        /// <param name="symptoms">List of symptoms</param>
        /// <returns>Instance of least popular symptom</returns>
        Symptom FindLeastPopularSymptom(List<Symptom> symptoms);

        /// <summary>
        /// Find all possible symptoms by filter
        /// </summary>
        /// <param name="accepted">List of symptoms that a person has</param>
        /// <param name="denied">List of symptoms that a person does not have</param>
        /// <returns>List of possible symptoms</returns>
        List<Symptom> FindPossibleSymptoms(List<Symptom> accepted, List<Symptom> denied);

        /// <summary>
        /// Find all possible diseases by filter
        /// </summary>
        /// <param name="accepted">List of symptoms that a person has</param>
        /// <param name="denied">List of symptoms that a person does not have</param>
        /// <returns>List of possible diseases</returns>
        List<Disease> FindPossibleDiseases(List<Symptom> accepted, List<Symptom> denied);
    }
}