﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using Domain;

namespace BLL
{
    public class HospitalService : IHospitalService
    {
        private readonly DataService _data;

        public HospitalService(string path)
        {
            _data = new DataService(path);
        }

        public List<Disease> GetPopularDiseases() => _data.Diseases.OrderByDescending(d => d.Symptoms.Count).ThenBy(d => d.DiseaseName).ToList().GetRange(0, 3);
        public int GetNumberOfUniqueSymptoms() => _data.Symptoms.Count;
        public List<Symptom> GetPopularSymptoms() => _data.Symptoms.OrderByDescending(s => s.Diseases.Count).ThenBy(s => s.SymptomName).ToList().GetRange(0, 3);
        public List<Disease> FindDiseaseBySymptoms(string[] symptoms) => FindDiseaseBySymptoms(symptoms.Select(s => _data.GetSymptom(s)).ToList());
        public List<Disease> FindDiseaseBySymptoms(List<Symptom> symptoms) => _data.Diseases.Where(d => symptoms.All(s => d.Symptoms.Contains(s))).ToList();

        public List<Disease> GetAllDiseases() => _data.Diseases; //Get disease collection from data service
        public List<Symptom> GetAllSymptoms() => _data.Symptoms; //Get symptom collection from data service

        public Symptom FindLeastPopularSymptom(List<Symptom> symptoms) => symptoms.OrderBy(s => s.Diseases.Count).FirstOrDefault();
        public List<Disease> FindPossibleDiseases(List<Symptom> accepted, List<Symptom> denied) => FindDiseaseBySymptoms(accepted).Where(d => denied.All(s => !d.Symptoms.Contains(s))).ToList();

        public List<Symptom> FindPossibleSymptoms(List<Symptom> accepted, List<Symptom> denied)
        {
            //var diseases = FindDiseaseBySymptoms(accepted).Where(d => denied.All(s => !d.Symptoms.Contains(s))).ToList(); //Find possible diseases
            var diseases = FindPossibleDiseases(accepted, denied); //Find possible diseases
            var output = new List<Symptom>();

            //Iterate through all possible diseases and adds symptoms to output(that are not already present in output or filter collections)
            foreach (var item in diseases)
            {
                foreach (var symp in item.Symptoms)
                {
                    if (output.Contains(symp) || accepted.Contains(symp) || denied.Contains(symp)) continue;
                    output.Add(symp);
                }
            }
            return output;
        }
    }
}