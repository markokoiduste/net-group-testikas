﻿using System.Collections.Generic;
using System.Linq;
using LINQtoCSV;

namespace Domain
{
    public class Disease
    {
        public int DiseaseId { get; set; }
        public string DiseaseName { get; set; }
        public virtual List<Symptom> Symptoms { get; set; } = new List<Symptom>();

        public override string ToString()
        {
            return $"{DiseaseName}";
        }
    }
}