﻿using System.Collections.Generic;

namespace Domain
{
    public class Symptom
    {
        public string SymptomName { get; set; }
        public virtual List<Disease> Diseases { get; set; } = new List<Disease>();

        public override string ToString()
        {
            return SymptomName;
        }
    }
}