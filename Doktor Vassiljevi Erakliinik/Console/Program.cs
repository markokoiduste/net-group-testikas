﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BLL;
using DAL;
using Domain;
using static System.Console;

namespace Console
{
    class Program
    {
        private static DateTime _start;
        static void Main(string[] args)
        {
            var path = @"C:\Users\Marko\Desktop\disease database.csv"; //"Hard-coded" path, is used if no argument is given.

            //First parameter should be a path
            if (args.Length > 0) path = args[0];
            while (!File.Exists(path)) path = AskPath(); //Path validation

            IHospitalService service = new HospitalService(path); //Start new Hospital service instance
            _start = DateTime.Now;
            Initialize(service); //Start program on console
        }

        /// <summary>
        /// Console section to ask user for database path
        /// </summary>
        /// <returns></returns>
        private static string AskPath()
        {
            WriteLine($"[{DateTime.Now}] Dr. Vassiljev: No database found. Enter correct path of .csv database");
            Write("Path: ");
            return ReadLine();
        }

        /// <summary>
        /// Program initialization. Once data is loaded it should be invoked.
        /// </summary>
        /// <param name="service">Hospital service with business logic</param>
        private static void Initialize(IHospitalService service)
        {
            //1.1
            WriteLine($"Popular diseases: ");
            WriteLineList(service.GetPopularDiseases());

            //1.2
            WriteLine($"There are {service.GetNumberOfUniqueSymptoms()} unique symptoms.");

            //1.3
            WriteLine($"Popular symptoms: ");
            WriteLineList(service.GetPopularSymptoms());

            AnyKeyPause();
            Menu(service);
        }

        /// <summary>
        /// Console section for menu
        /// </summary>
        /// <param name="service">Hospital service with business logic</param>
        private static void Menu(IHospitalService service)
        {
            //Clear();
            //WriteLine($"--- Menu ---");
            //WriteLine($"Options: [I]nteractive Dr. Vassiljev [F]ind disease by symptom(s) [E]xit");
            //var key = ReadKey(true).Key;
            while (true)
            {
                Clear();
                WriteLine($"--- Menu ---");
                WriteLine($"Options: [I]nteractive Dr. Vassiljev [F]ind disease by symptom(s) [E]xit");
                var key = ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.F:
                        FindDiseaseBySymptoms(service);
                        break;

                    case ConsoleKey.I:
                        InteractiveDoctor(service);
                        break;

                    case ConsoleKey.E:
                        ConfirmExit();
                        break;

                    default:
                        continue;
                }
            }
        }

        /// <summary>
        /// Console section for finding disease by symptoms
        /// </summary>
        /// <param name="service">Hospital service with business logic</param>
        private static void FindDiseaseBySymptoms(IHospitalService service)
        {
            WriteLine($"--- Find disease by symptoms ---");
            WriteLine($"Select by inserting index numbers, separate multiselection with commas(,)");
            WriteLine();

            var symptoms = service.GetAllSymptoms();
            foreach (var item in symptoms) WriteLine($"[{symptoms.IndexOf(item)}]{item} ");

            var input = ReadLine();
            try
            {
                var selection = input?.Split(',').Select(i => symptoms.ElementAt(int.Parse(i.Trim()))).Distinct().ToList();
                Write("Selected: ");
                WriteList(selection);
                WriteLine();

                var result = service.FindDiseaseBySymptoms(selection);
                if (result.Count > 0)
                {
                    WriteLine($"Found {result.Count} matching diseases: ");
                    WriteLineList(result);
                }
                else
                {
                    WriteLine($"No matching diseases found");
                }
            }
            catch (Exception ex)
            {
                WriteLine("Faulty input");
            }
            AnyKeyPause("Find disease by Symptoms finished it's job");  
        }

        /// <summary>
        /// Console section for interactive doctor
        /// </summary>
        /// <param name="service">Hospital service with business logic</param>
        private static void InteractiveDoctor(IHospitalService service)
        {
            WriteLine("--- Dr. Vassiljev's interactive assistant ---");
            var symptoms = service.GetAllSymptoms();
            var diseases = service.GetAllDiseases();
            var check = service.FindLeastPopularSymptom(symptoms);
            var selected = new List<Symptom>();
            var denied = new List<Symptom>();
            while (diseases.Count > 1 && symptoms.Count > 1)
            {
                WriteLine($"Do you have {check}?  (Y/N) {diseases.Count}  {symptoms.Count}");
                var key = ReadKey(true).Key;
                if (key == ConsoleKey.Y) selected.Add(check);
                if (key == ConsoleKey.N) denied.Add(check);
                symptoms = service.FindPossibleSymptoms(selected, denied);
                diseases = service.FindPossibleDiseases(selected, denied);
                check = service.FindLeastPopularSymptom(symptoms);
            }

            if (diseases.Count < 1)
            {
                WriteLine("No matching disease found");
            }
            else
            {
                WriteLine("Possible diseases: ");
                WriteLineList(diseases);
            }
            AnyKeyPause("Interactive doctor finished it's job");
        }

        #region Helpers

        /// <summary>
        /// Method to ask exit confirmation before exiting the program
        /// </summary>
        private static void ConfirmExit()
        {
            while (true)
            {
                Clear();
                WriteLine("Confirm exit? Y / N");
                var answer = ReadKey(true).Key;
                if (answer == ConsoleKey.N) return;
                if (answer == ConsoleKey.Y) Exit();
            }
        }

        /// <summary>
        /// Exit method. Clears window, shows info regarding uptime and exits the program.
        /// </summary>
        private static void Exit()
        {
            Clear();
            WriteLine($"[{DateTime.Now}] Program terminated | Uptime: {DateTime.Now - _start}");
            Environment.Exit(0);
        }

        /// <summary>
        /// Helper to throw "Press any key" pause in program
        /// </summary>
        /// <param name="message">Custom message to be shown before the pause</param>
        private static void AnyKeyPause(string message)
        {
            if (message.Length > 0) WriteLine($"{message}");
            AnyKeyPause();
        }

        /// <summary>
        /// Helper to throw "Press any key" pause in program
        /// </summary>
        private static void AnyKeyPause()
        {
            WriteLine("Press any key to continue . . .");
            ReadKey();
        }

        /// <summary>
        /// Helper to write a collection as list using linebreaks
        /// </summary>
        /// <typeparam name="T">Type of data</typeparam>
        /// <param name="list">Data collection</param>
        private static void WriteLineList<T>(IEnumerable<T> list)
        {
            foreach (var item in list)
            {
                WriteLine($"{null, 5}{item} ");
            }
        }

        /// <summary>
        /// Helper to write a collection as list
        /// </summary>
        /// <typeparam name="T">Type of data</typeparam>
        /// <param name="list">Data collection</param>
        private static void WriteList<T>(IEnumerable<T> list)
        {
            foreach (var item in list)
            {
                Write($"{null, 2}{item} ");
            }
        }
        #endregion
    }
}
